## Valtrans Task

### Tech
- MVVM Architecture
- Kotlin
- JetPack Architecture components
- Retrofit

### Apk Download
[Release APK](https://gitlab.com/shahzar/valtrans/raw/master/app/release/app-release.apk)