package com.shahzar.valtranstask.data

import android.util.Log
import com.shahzar.valtranstask.data.model.LoginResponse
import com.shahzar.valtranstask.data.repository.ApiService
import okhttp3.MediaType
import okhttp3.RequestBody
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response

class DataManager {

    private val deviceId = "TEST"
    private val deviceName = "android"

    private val apiService = ApiService.create()

    fun loginUser(username: String, password: String, onResponse: (LoginResponse)->Unit, onFailure: ()->Unit) {

        val usernameRequestBody = RequestBody.create(MediaType.parse("text/plain"), username)
        val passwordRequestBody = RequestBody.create(MediaType.parse("text/plain"), password)
        val deviceIdRequestBody = RequestBody.create(MediaType.parse("text/plain"), deviceId)
        val deviceNameRequestBody = RequestBody.create(MediaType.parse("text/plain"), deviceName)

        apiService.login(usernameRequestBody, passwordRequestBody, deviceIdRequestBody, deviceNameRequestBody)
            .enqueue(object : Callback<LoginResponse> {

                override fun onFailure(call: Call<LoginResponse>, t: Throwable) {
                    t.printStackTrace()
                    onFailure()
                }

                override fun onResponse(call: Call<LoginResponse>, response: Response<LoginResponse>) {
                    response.body()?.let {
                        onResponse(it)
                    }
                    Log.d("Valtrans", "Login success: ${response.body()?.status}")
                }
            })
    }

}