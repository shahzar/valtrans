package com.shahzar.valtranstask.data.repository

import com.shahzar.valtranstask.data.model.LoginResponse
import okhttp3.RequestBody
import retrofit2.Call
import retrofit2.Retrofit
import retrofit2.converter.gson.GsonConverterFactory
import retrofit2.http.GET
import retrofit2.http.Multipart
import retrofit2.http.POST
import retrofit2.http.Part

interface ApiService {

    companion object {

        private val BASE_URL = "https://dev.parkonic.com/api/"

        fun create() : ApiService {
            val retrofit = Retrofit.Builder()
                .addConverterFactory(GsonConverterFactory.create())
                .baseUrl(BASE_URL)
                .build()
            return retrofit.create(ApiService::class.java)
        }
    }


    @Multipart
    @POST("app/user-login")
    fun login(@Part("user_email") user_email: RequestBody,
              @Part("user_password") user_password: RequestBody,
              @Part("user_device_id") user_device_id: RequestBody,
              @Part("user_device_name") user_device_name: RequestBody): Call<LoginResponse>
}