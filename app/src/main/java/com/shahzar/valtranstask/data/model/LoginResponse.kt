package com.shahzar.valtranstask.data.model


import com.google.gson.annotations.SerializedName
import java.io.Serializable

data class LoginResponse(@SerializedName("data")
                         val data: Data?,
                         @SerializedName("status")
                         val status: Boolean = false,
                         val message: String = ""): Serializable


data class Data(@SerializedName("user_email")
                val userEmail: String = "",
                @SerializedName("user_id")
                val userId: Int = 0,
                @SerializedName("user_name")
                val userName: String = "",
                @SerializedName("is_email_verified")
                val isEmailVerified: Boolean = false,
                @SerializedName("user_remaining_balance")
                val userRemainingBalance: Int = 0,
                @SerializedName("user_phone")
                val userPhone: String = "",
                @SerializedName("user_has_vehicle")
                val userHasVehicle: Boolean = false,
                @SerializedName("user_photo")
                val userPhoto: String = "",
                @SerializedName("user_has_tag")
                val userHasTag: Boolean = false,
                @SerializedName("token")
                val token: String = ""): Serializable


