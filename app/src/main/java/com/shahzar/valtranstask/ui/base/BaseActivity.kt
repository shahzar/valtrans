package com.shahzar.valtranstask.ui.base

import androidx.appcompat.app.AppCompatActivity

open class BaseActivity: AppCompatActivity()