package com.shahzar.valtranstask.ui

import android.util.Log
import androidx.fragment.app.FragmentActivity
import com.shahzar.valtranstask.R
import com.shahzar.valtranstask.ui.base.BaseFragment

class NavMgr {

    companion object {
        val instance = NavMgr()
    }

    fun pushFragment(activity: FragmentActivity?, fragment: BaseFragment) {
        activity
            ?.supportFragmentManager
            ?.beginTransaction()
            ?.addToBackStack("")
            ?.replace(R.id.content_frag, fragment)
            ?.commit()
        Log.d("NavMgr", "Pushed fragment ${fragment.javaClass.simpleName}")
    }

    fun pop(activity: FragmentActivity?) {
        activity?.supportFragmentManager?.popBackStack()
        Log.d("NavMgr", "Popped fragment")
    }

    fun hasItemsInBackstack(activity: FragmentActivity?) : Boolean{

        activity?.let {
            return it.supportFragmentManager.backStackEntryCount > 1
        }

        return false
    }

}