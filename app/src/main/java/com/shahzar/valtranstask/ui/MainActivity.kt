package com.shahzar.valtranstask.ui

import android.os.Bundle
import com.shahzar.valtranstask.R
import com.shahzar.valtranstask.ui.base.BaseActivity
import com.shahzar.valtranstask.ui.login.LoginFragment

class MainActivity : BaseActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        if (!NavMgr.instance.hasItemsInBackstack(this)) {
            NavMgr.instance.pushFragment(this, LoginFragment.newInstance())
        }
    }

    override fun onBackPressed() {

        if (!NavMgr.instance.hasItemsInBackstack(this)) {
            finish()
            return
        }

        super.onBackPressed()
    }
}
