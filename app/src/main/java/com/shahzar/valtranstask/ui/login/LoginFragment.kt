package com.shahzar.valtranstask.ui.login

import androidx.lifecycle.ViewModelProviders
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.lifecycle.Observer

import com.shahzar.valtranstask.R
import com.shahzar.valtranstask.ui.NavMgr
import com.shahzar.valtranstask.ui.base.BaseFragment
import com.shahzar.valtranstask.ui.home.HomeFragment
import kotlinx.android.synthetic.main.login_fragment.view.*

class LoginFragment : BaseFragment() {

    companion object {
        fun newInstance() = LoginFragment()
    }

    private lateinit var viewModel: LoginViewModel
    private lateinit var rootView: View

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?
    ): View? {

        rootView = inflater.inflate(R.layout.login_fragment, container, false)
        return rootView
    }

    override fun onActivityCreated(savedInstanceState: Bundle?) {
        super.onActivityCreated(savedInstanceState)
        viewModel = ViewModelProviders.of(this).get(LoginViewModel::class.java)
        initView()
    }

    fun initView() {

        setTitle(resources.getString(R.string.page_title_login))

        rootView.btn_login.setOnClickListener {
            hideKeyboard()
            viewModel.loginUser(rootView.username_edit_text.text.toString(), rootView.password_edit_text.text.toString())
        }

        viewModel.loginResponse.observe(viewLifecycleOwner, Observer {
            if(it?.data != null) {
                NavMgr.instance.pushFragment(activity, HomeFragment.newInstance(it))
            } else {
                showMessage(rootView, getString(R.string.err_login_failed))
            }
        })

        viewModel.progressLiveData.observe(viewLifecycleOwner, Observer {
            rootView.progress_bar.visibility = if (it) View.VISIBLE else View.GONE
        })

    }

}
