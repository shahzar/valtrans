package com.shahzar.valtranstask.ui.base

import android.app.Activity
import android.view.View
import android.view.inputmethod.InputMethodManager
import androidx.appcompat.app.AppCompatActivity
import androidx.fragment.app.Fragment
import com.google.android.material.snackbar.Snackbar
import com.shahzar.valtranstask.ui.NavMgr

open class BaseFragment: Fragment() {

    fun goBack() {
        NavMgr.instance.pop(activity)
    }

    fun showError(view: View, msg: String) {
        Snackbar.make(view, msg, Snackbar.LENGTH_LONG).show()
    }

    fun showMessage(view: View?, msg: String) {
        view?.let {
            Snackbar.make(it, msg, Snackbar.LENGTH_LONG).show()
        }
    }

    fun setTitle(title: String) {
        activity?.let { (it as AppCompatActivity).supportActionBar?.title = title }
    }

    fun hideKeyboard() {
        view?.let {
            val inputMethodManager = activity?.getSystemService(Activity.INPUT_METHOD_SERVICE) as InputMethodManager
            inputMethodManager.hideSoftInputFromWindow(it.windowToken, 0)
        }
    }
}