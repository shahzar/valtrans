package com.shahzar.valtranstask.ui.login

import android.util.Log
import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import com.hadilq.liveevent.LiveEvent
import com.shahzar.valtranstask.data.DataManager
import com.shahzar.valtranstask.data.model.LoginResponse

class LoginViewModel : ViewModel() {

    private val _loginSuccess = LiveEvent<LoginResponse>()
    private val _progressLiveData = MutableLiveData<Boolean>()

    val loginResponse: LiveData<LoginResponse>
        get() = _loginSuccess

    val progressLiveData: LiveData<Boolean>
        get() = _progressLiveData

    fun loginUser(username: String, pass: String) {

        _progressLiveData.value = true

        DataManager().loginUser(
            username,
            pass,
            {
                _loginSuccess.value = it
                _progressLiveData.value = false
            },
            {
                _loginSuccess.value = null
                _progressLiveData.value = false
            })

    }
}
