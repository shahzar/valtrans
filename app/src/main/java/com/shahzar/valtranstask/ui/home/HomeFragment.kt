package com.shahzar.valtranstask.ui.home

import androidx.lifecycle.ViewModelProviders
import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup

import com.shahzar.valtranstask.R
import com.shahzar.valtranstask.data.model.LoginResponse
import com.shahzar.valtranstask.ui.base.BaseFragment
import kotlinx.android.synthetic.main.home_fragment.*
import kotlinx.android.synthetic.main.home_fragment.view.*

class HomeFragment() : BaseFragment() {

    companion object {
        fun newInstance(loginResponse: LoginResponse) = HomeFragment().apply {
            arguments = Bundle().apply {
                putSerializable("login_response", loginResponse)
            }
        }
    }

    private lateinit var viewModel: HomeViewModel
    private lateinit var rootView: View

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        rootView = inflater.inflate(R.layout.home_fragment, container, false)
        return rootView
    }

    override fun onActivityCreated(savedInstanceState: Bundle?) {
        super.onActivityCreated(savedInstanceState)
        viewModel = ViewModelProviders.of(this).get(HomeViewModel::class.java)
        initView()
    }

    fun initView() {

        setTitle(resources.getString(R.string.page_title_home))

        val loginResponse = arguments?.getSerializable("login_response") as LoginResponse

        loginResponse.data?.apply {
            rootView.username_text_view.text = userName
            rootView.phnum_text_view.text = userPhone
        }
    }

}
